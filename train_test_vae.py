#!/usr/bin/env python
# coding: utf-8
import torch
from torch import nn, optim
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader
from torchvision.utils import save_image
from torchvision import transforms, datasets
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import argparse
import os

from utils.data_loader import CustomDataset
from models.vae import VAE

parser = argparse.ArgumentParser(description='Train or test VAE model.')
parser.add_argument('--train', '-t', action='store_true',
                    help='Goes into training mode, otherwise tests model.')

parser.add_argument('--epochs', '-e', type=int, default=10, metavar='int', dest='epochs',
                    help='Train: Set the number of epochs to train.')

parser.add_argument('--path_train_data', '-trp', type=str, default='data', metavar='FOLDER', dest='train_path',
                    help='Train: Folder which contains training images')

parser.add_argument('--save_model', '-sm', type=int, default=20, metavar='int x', dest='save_model',
                    help='Train: Saves model checkpoint every x epochs, default 20.')

parser.add_argument('--path_test_data', '-tep', type=str, default='data', metavar='FOLDER', dest='test_path',
                    help='Test: Folder which contains test images')

parser.add_argument('--model_file', '-mf', type=str, default='models/vae20.pt', metavar='FILE', dest='model_path',
                    help='Test: Specify the path to saved model for testing. Default models/vae20.pt')

parser.add_argument('--batchsize', '-bs', type=int, default=32, metavar='int', dest='batchsize',
                    help='Optional: set the batch size.') 

parser.add_argument('--latentdim', '-z', type=int, default=32, metavar='int', dest='latentdim',
                    help='Optional: set the latent dimension size of z for model.')

parser.add_argument('--learning_rate', '-lr', type=float, default=0.001, metavar='float', dest='lr',
                    help='Optional: set the learning rate for training.')

parser.add_argument('--save_imgs', '-si', type=int, default=None, metavar='int x', dest='save_imgs',
                    help='Optional: Saves images every x epochs. If testing, enter any number to save generated images. ')


arg = parser.parse_args()

# config
save_path = 'model_ckpts/' # folder to save checkpoints
img_save_path = 'imgs/'
img_batch = 5 # batch num to save for images
plt_loss = False # change to plot loss at end of training

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# LOAD DATA -- change transforms in data_loader.py

# training data is randomly flipped 
train_dataset = CustomDataset(arg.train_path)
images  = train_dataset.get_image_data(augment=False)
loader = train_dataset.get_data_loader(images, batch_size=arg.batchsize)

# test data is flipped and center cropped 
test_dataset = CustomDataset(arg.test_path)
test_images = test_dataset.get_image_data(augment=True)
test_loader = test_dataset.get_data_loader(test_images, batch_size=arg.batchsize)

print('Num training imgs: ', len(images))

# make instance of vae
vae = VAE(latent_dim=arg.latentdim, kl_tolerance=0.5).to(device)
optimizer = optim.Adam(vae.parameters(), lr=arg.lr)
toim = transforms.ToPILImage()

def train_vae(epochs, plt_loss=True):
    # saves the batch of generated images every save_im_epochs num epochs 
    vae.train()
    losses = []
    
    for epoch in range(1, epochs+1):
        total_loss = 0
        out_imgs = []
        # real_imgs = []
        for databatch in loader:
            databatch = databatch.to(device)
            optimizer.zero_grad()

            z, data_reconstructed, loss = vae.forward_VAE(databatch)

            # save images
            out_imgs.append(data_reconstructed)
            # real_imgs.append(databatch)

            loss.backward()

            total_loss += loss.item()

            optimizer.step()
           
        
        avg_img_loss = total_loss/(len(images))
        losses.append(avg_img_loss) # for graph
        print('Epoch: {}   Avg Loss: {}'.format(epoch, avg_img_loss)) 

        if arg.save_imgs is not None:
            if not os.path.exists(img_save_path):
                os.mkdir(img_save_path)
            if epoch%arg.save_imgs == 0:
                whichbatch = out_imgs[img_batch] # which batch number to save
                for i, image in enumerate(whichbatch):
                    img = toim(image)
                    img = img.resize((320,240), Image.ANTIALIAS)
                    img.save(img_save_path + 'generated' + str(epoch) + '_' + str(i) + '.jpg')
        # save real images for comparison
        # if epoch ==1 :
        #     whichbatch = real_imgs[img_batch]
        #     for i, image in enumerate(whichbatch):
        #         img = toim(image)
        #         img = img.resize((320,240), Image.ANTIALIAS)
        #         img.save('./imgs/real' + str(epoch) + '_' + str(i) + '.jpg')


        # save model 
        if not os.path.exists(save_path):
            os.mkdir(save_path)
        path = save_path + 'vae' + str(epoch) + '.pt'
        if epoch%arg.save_model == 0:
            torch.save({
            'epoch': epoch,
            'model_state_dict': vae.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'loss': loss
            }, path)

    if plt_loss:
        # plot loss
        plt.plot(losses)
        plt.xlabel("Epoch")
        plt.ylabel("Avg. Loss")
        plt.savefig("loss.png")
        plt.show()
    


def test_vae(model_path, save_input=False, saved_model = True):
    # specify path to saved model, whether to save input images, default uses saved model
    if saved_model: 
        ckpt= torch.load(model_path)
        vae.load_state_dict(ckpt['model_state_dict'])
        optimizer.load_state_dict(ckpt['optimizer_state_dict'])
        epoch = ckpt['epoch']
        loss = ckpt['loss']

    vae.eval()
    out_imgs = []
    real_imgs = []
    total_loss=0
    with torch.no_grad():
        for databatch in test_loader:
            databatch = databatch.to(device)

            z, data_reconstructed, loss = vae.forward_VAE(databatch)

            if save_input:
                real_imgs.append(databatch)
            if arg.save_imgs is not None:
                out_imgs.append(data_reconstructed)

            total_loss += loss.item()


        avg_img_loss = total_loss/(len(test_images))
        print('Avg. Loss: ', avg_img_loss)

        if arg.save_imgs is not None:
            if not os.path.exists(img_save_path):
                os.mkdir(img_save_path)
            whichbatch = out_imgs[img_batch] # which batch number to save
            for i, image in enumerate(whichbatch):
                img = toim(image)
                img = img.resize((320,240), Image.ANTIALIAS)
                img.save(img_save_path + 'testoutput' + str(epoch) + '_' + str(i) + '.jpg')
        if save_input:
            realbatch = real_imgs[img_batch]
            for i, image in enumerate(realbatch):
                img = toim(image)
                img = img.resize((320,240), Image.ANTIALIAS)
                img.save(img_save_path + 'real' + str(epoch) + '_' + str(i) + '.jpg')





if __name__ == "__main__":
    if arg.train:
        train_vae(epochs=arg.epochs, plt_loss=plt_loss)
    else:
        test_vae(model_path=arg.model_path, save_input=False)

