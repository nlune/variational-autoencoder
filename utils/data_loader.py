from torch.utils.data.dataset import Dataset
from torchvision import transforms
from PIL import Image
import os
import numpy as np
import torch
import matplotlib.pyplot as plt


class CustomDataset(Dataset):
    def __init__(self, image_dir):
        # for train images which are resized & randomly flipped
        self.transform = transforms.Compose([ transforms.RandomHorizontalFlip(),
                                     # transforms.RandomCrop(240),
                                transforms.Resize((64,64)),
                                transforms.ToTensor()])
        self.image_dir = image_dir
        self.images = None
        self.loader = None


    def get_image_data(self, augment = False):
        # augment generates test images which are horizontally flipped and center cropped
        images = []
        test_images = []
        for filename in os.listdir(self.image_dir):
            img = Image.open(os.path.join(self.image_dir,filename))
            if self.transform is not None:
                img1 = self.transform(img)
            images.append(img1)

            if augment:
                transform2 = transforms.Compose([ transforms.RandomHorizontalFlip(1),
                                        transforms.CenterCrop(240),
                                        transforms.Resize((64,64)),
                                        transforms.ToTensor()])
                img2 = transform2(img)
                test_images.append(img2)


        self.images = images 

        if augment:
            return test_images


        return images

    def get_data_loader(self, data, batch_size = 32, shuffle = False):
        data_loader = torch.utils.data.DataLoader(dataset= data,
                                                batch_size= batch_size,
                                                shuffle= shuffle)

        self.loader = data_loader # best way to use class functions or directly access var? 

        return data_loader


if __name__ == '__main__':
    custom_dataset = CustomDataset('data')
    images = custom_dataset.get_image_data(augment=False)
    loader = custom_dataset.get_data_loader(images)

    print(len(custom_dataset.images))
    print(images[0].size())

    toim = transforms.ToPILImage()
    for i_batch, sample in enumerate(loader):
        print(i_batch, print(sample.size()))
        if i_batch == 26:
            for i,s in enumerate(sample):
                im = toim(s)
                im = im.resize((320,240), Image.ANTIALIAS)
                im.save('./imgs/' + str(i) + '.jpeg')
                

        






