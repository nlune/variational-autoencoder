# Variational Auto-Encoder

This Pytorch VAE follows the structure of the Vision Model in [World Models](https://worldmodels.github.io/). To run the VAE training and testing, first install the requirements:

`pip install -r requirements.txt`

Place your training images and test images in separate folders. To change the transforms used on the dataset, you can edit the CustomDataset class in utils/data_loader.py.

To look at the different options, run:    
`./train_test_vae.py --help `

## Training
To train the model, run `./train_test_vae.py --train`  along with folder containing training dataset, etc.

Eg. `./train_test_vae.py -t --epochs 20 --path_train_data data_folder --save_model 20`

Models will be saved in the folder 'model_ckpts', and saved images in the folder 'imgs'.


## Testing
To test the model after training, run `./train_test_vae.py` along with folder path to saved model, etc.

Eg. `./train_test_vae.py  --path_test_data data_folder  --model_file model_ckpts/vae20.pt`


To save a batch of generated images after every, say 10 epochs of training, add ` --save_imgs 10`.
For saving generated images from testing, just put any number.
To change the batch number to be saved, simply change img_batch variable in train_test_vae.py.  


## Results using toy data
Using a short video clip from the [Rawseeds Project](http://www.rawseeds.org/home/), of 851 frames, the VAE was trained for 30 epochs to generate the following output (a sample batch of 32 frames):

![Generated Images](display/generated.gif)

Compared to the original:

![Real Images](display/real.gif)


The evolution over 30 epochs:

![output over 30 epochs](display/30epochs.gif)
