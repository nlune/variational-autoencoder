#!/usr/bin/env python
# coding: utf-8

import torch
from torch import nn, optim
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader
from torchvision.utils import save_image
import matplotlib.pyplot as plt
from torchvision import transforms, datasets
import numpy as np
from PIL import Image
from utils.data_loader import CustomDataset

# 320x240 or 640x480 original data
# compressed to 64x64 for model

class VAE(nn.Module):
    def __init__(self, img_channels=3, latent_dim=32, kl_tolerance=0.5):
        super(VAE, self).__init__()
        self.kl_tolerance = kl_tolerance

        # encoder hidden layers
        self.conv1 = nn.Conv2d(in_channels=img_channels, out_channels=32, kernel_size=4, stride=2) 
        self.conv2 = nn.Conv2d(32, 64, 4, stride=2) # output size is 1/2 - 1 
        self.conv3 = nn.Conv2d(64, 128, 4, stride=2)
        self.conv4 = nn.Conv2d(128, 256, 4, stride=2)
        
        # encoder output layers
        self.fcmean = nn.Linear(in_features=4*256, out_features=latent_dim) # ends up w/ 2x2, valid padded
        self.fclogvar = nn.Linear(4*256, latent_dim)
        
        # decoder hidden layers 
        self.fc = nn.Linear(in_features=latent_dim, out_features=1024)
        self.deconv1 = nn.ConvTranspose2d(in_channels=1024, out_channels=128, kernel_size=5, stride=2)
        self.deconv2 = nn.ConvTranspose2d(128, 64, 5, stride=2)
        self.deconv3 = nn.ConvTranspose2d(64, 32, 6, stride=2)
        # decoder output layer
        self.deconv4 = nn.ConvTranspose2d(32, img_channels, 6, stride=2)
    
    
    def forward_encode(self, x):
        # outputs mean and logvar vectors [batch_size, latent_dim]
        h = F.relu(self.conv1(x))
        h = F.relu(self.conv2(h))
        h = F.relu(self.conv3(h))
        h = F.relu(self.conv4(h))
        
        # reshape
        h = h.view(h.size(0), -1) # keep 1st dim, infer rest
        
        mu = self.fcmean(h)
        logvar = self.fclogvar(h)
        
        return mu, logvar
    
    
    def get_z(self, mu, logvar):
        # reparameterization
        sigma = (logvar).exp() 
        sigma = torch.sqrt(sigma)  # slightly worse than w/out sqrt using variance, why ? 
        eps = torch.rand(*mu.shape)

        z = eps*sigma + mu
        
        return z
    
    
    def forward_decode(self, z):
        h = F.relu(self.fc(z))

        # reshape
        h = h.view(*h.shape, 1, 1 ) # shape [32, 1024, 1, 1]

        
        h = F.relu(self.deconv1(h))
        h = F.relu(self.deconv2(h))
        h = F.relu(self.deconv3(h))
        
        x_hat = torch.sigmoid(self.deconv4(h))
        
        return x_hat

    def forward_VAE(self, databatch):
        # encoder output
        mus, logvars = self.forward_encode(databatch) 

        # reparameterization trick to get z
        z = self.get_z(mus, logvars)

        # decoder output
        data_reconstructed = self.forward_decode(z)

        loss = self.get_loss(data_reconstructed, databatch, mus, logvars)

        return z, data_reconstructed, loss


    def get_loss(self, x_hat, x, mu, logvar):
        # reconstruction loss
        r_loss = F.mse_loss(x_hat, x, reduction='sum')
        kl_loss = - self.kl_tolerance * torch.sum(1 + logvar - mu**2 - logvar.exp()) # why does reimplementation use 2*logsigma?

        total_loss = r_loss + kl_loss

        return total_loss









   

